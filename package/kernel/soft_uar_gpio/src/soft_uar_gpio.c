#include <linux/delay.h>
#include <linux/module.h>
#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/version.h>
#include <linux/gpio.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/interrupt.h>

#define SOFT_UAR_MAJOR            0
#define N_PORTS                    1
#define NONE                       0

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dmitry Peresypkin");
MODULE_DESCRIPTION("Software-UAR");

static int gpio_rx = 16;
module_param(gpio_rx, int, 0);

// Module prototypes.
static int  soft_uar_open(struct tty_struct*, struct file*);
static void soft_uar_close(struct tty_struct*, struct file*);
static int  soft_uar_write(struct tty_struct*, const unsigned char*, int);
static void soft_uar_flush_buffer(struct tty_struct*);
static void soft_uar_set_termios(struct tty_struct*, struct ktermios*);
static void soft_uar_stop(struct tty_struct*);
static void soft_uar_start(struct tty_struct*);
static void soft_uar_hangup(struct tty_struct*);
static int  soft_uar_tiocmget(struct tty_struct*);
static int  soft_uar_tiocmset(struct tty_struct*, unsigned int, unsigned int);
static int  soft_uar_ioctl(struct tty_struct*, unsigned int, unsigned int long);
static void soft_uar_throttle(struct tty_struct*);
static void soft_uar_unthrottle(struct tty_struct*);

static irq_handler_t handle_rx_start(unsigned int irq, void* device, struct pt_regs* registers);
static enum hrtimer_restart handle_rx(struct hrtimer* timer);
static void receive_character(unsigned char character);

// Module operations.
static const struct tty_operations soft_uar_operations = {
    .open            = soft_uar_open,
    .close           = soft_uar_close,
    .write           = soft_uar_write,
    .flush_buffer    = soft_uar_flush_buffer,
    .ioctl           = soft_uar_ioctl,
    .set_termios     = soft_uar_set_termios,
    .stop            = soft_uar_stop,
    .start           = soft_uar_start,
    .hangup          = soft_uar_hangup,
    .tiocmget        = soft_uar_tiocmget,
    .tiocmset        = soft_uar_tiocmset,
    .throttle        = soft_uar_throttle,
    .unthrottle      = soft_uar_unthrottle
};

// Driver instance.
static struct tty_driver* soft_uar_driver = NULL;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
static struct tty_port port;
#endif

static struct tty_struct* current_tty = NULL;
static DEFINE_MUTEX(current_tty_mutex);
static struct hrtimer timer_rx;
static ktime_t period;
static int rx_bit_index = -1;

/**
 * Initializes the Soft UAR infrastructure.
 * This must be called during the module initialization.
 * The GPIO pin used as RX is configured as input.
 * @param gpio_rx GPIO pin used as RX
 * @return 1 if the initialization is successful. 0 otherwise.
 */
static int __soft_uar_init(const int _gpio_rx)
{
    bool success = true;

    mutex_init(&current_tty_mutex);

    // Initializes the RX timer.
    hrtimer_init(&timer_rx, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
    timer_rx.function = &handle_rx;

    // Initializes the GPIO pins.
    gpio_rx = _gpio_rx;
    printk(KERN_INFO "soft_uar: gpio_rx: %i\n", gpio_rx);

    success &= gpio_request(gpio_rx, "soft_uar_rx") == 0;
    success &= gpio_direction_input(gpio_rx) == 0;

    // Initializes the interruption.
    success &= request_irq(
        gpio_to_irq(gpio_rx),
        (irq_handler_t) handle_rx_start,
        IRQF_TRIGGER_FALLING,
        "soft_uar_irq_handler",
        NULL) == 0;

    disable_irq(gpio_to_irq(gpio_rx));

    return success;
}

/**
 * Finalizes the Soft UAR infrastructure.
 */
static int __soft_uar_finalize(void)
{
    free_irq(gpio_to_irq(gpio_rx), NULL);
    gpio_free(gpio_rx);
    return 1;
}

/**
 * Opens the Soft UAR.
 * @param tty
 * @return 1 if the operation is successful. 0 otherwise.
 */
static int __soft_uar_open(struct tty_struct* tty)
{
    int success = 0;
    mutex_lock(&current_tty_mutex);
    if (current_tty == NULL) {
        current_tty = tty;
        success = 1;
        rx_bit_index = -1;
        enable_irq(gpio_to_irq(gpio_rx));
    }
    mutex_unlock(&current_tty_mutex);
    return success;
}

/**
 * Closes the Soft UAR.
 */
static int __soft_uar_close(void)
{
    int success = 0;
    mutex_lock(&current_tty_mutex);
    if (current_tty != NULL) {
        disable_irq(gpio_to_irq(gpio_rx));
        hrtimer_cancel(&timer_rx);
        current_tty = NULL;
        success = 1;
    }
    mutex_unlock(&current_tty_mutex);
    return success;
}

/**
 * Sets the Soft UAR baudrate.
 * @param baudrate desired baudrate
 * @return 1 if the operation is successful. 0 otherwise.
 */
static int __soft_uar_set_baudrate(const int baudrate) 
{
    period = ktime_set(0, 1000000000/baudrate);
    gpio_set_debounce(gpio_rx, 1000/baudrate/2);
    return 1;
}


//-----------------------------------------------------------------------------
// Internals
//-----------------------------------------------------------------------------

/**
 * If we are waiting for the RX start bit, then starts the RX timer. Otherwise,
 * does nothing.
 */
static irq_handler_t handle_rx_start(unsigned int irq, void* device, struct pt_regs* registers)
{
    if (rx_bit_index == -1) {
        hrtimer_start(&timer_rx, ktime_set(0, ktime_to_ns(period) / 2), HRTIMER_MODE_REL);
    }
    return (irq_handler_t) IRQ_HANDLED;
}

/*
 * Receives a character and sends it to the kernel.
 */
static enum hrtimer_restart handle_rx(struct hrtimer* timer)
{
    ktime_t current_time = ktime_get();
    static unsigned int character = 0;
    int bit_value;
    enum hrtimer_restart result = HRTIMER_NORESTART;
    bool must_restart_timer = false;

    bit_value = gpio_get_value(gpio_rx);

    if (rx_bit_index == -1) {
        rx_bit_index++;
        character = 0;
        must_restart_timer = true;
    } else if (0 <= rx_bit_index && rx_bit_index < 8) {
        if (bit_value == 0) {
            character &= 0xfeff;
        } else {
            character |= 0x0100;
        }

        rx_bit_index++;
        character >>= 1;
        must_restart_timer = true;
    } else if (rx_bit_index == 8) {
        receive_character(character);
        rx_bit_index = -1;
    }

    // Restarts the RX timer.
    if (must_restart_timer) {
        hrtimer_forward(&timer_rx, current_time, period);
        result = HRTIMER_RESTART;
    }

    return result;
}

/**
 * Adds a given (received) character to the RX buffer, which is managed by the kernel,
 * and then flushes (flip) it.
 * @param character given character
 */
void receive_character(unsigned char character)
{
    mutex_lock(&current_tty_mutex);
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
    if (current_tty != NULL && current_tty->port != NULL) {
        tty_insert_flip_char(current_tty->port, character, TTY_NORMAL);
        tty_flip_buffer_push(current_tty->port);
    }
#else
    if (tty != NULL) {
        tty_insert_flip_char(current_tty, character, TTY_NORMAL);
        tty_flip_buffer_push(tty);
    }
#endif
    mutex_unlock(&current_tty_mutex);
}

/**
 * Module initialization.
 */
static int __init soft_uar_init(void)
{
    printk(KERN_INFO "soft_uar: Initializing module...\n");

    if (!__soft_uar_init(gpio_rx)) {
        printk(KERN_ALERT "soft_uar: Failed initialize GPIO.\n");
        return -ENOMEM;
    }

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
    printk(KERN_INFO "soft_uar: LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0).\n");

    // Initializes the port.
    tty_port_init(&port);
    port.low_latency = 0;

    // Allocates the driver.
    soft_uar_driver = tty_alloc_driver(N_PORTS, TTY_DRIVER_REAL_RAW);

    // Returns if the allocation fails.
    if (IS_ERR(soft_uar_driver)) {
        printk(KERN_ALERT "soft_uar: Failed to allocate the driver.\n");
        return -ENOMEM;
    }
#else
    printk(KERN_INFO "soft_uar: LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0).\n");

    // Allocates the driver.
    soft_uar_driver = alloc_tty_driver(N_PORTS);

    // Returns if the allocation fails.
    if (!soft_uar_driver) {
        printk(KERN_ALERT "soft_uar: Failed to allocate the driver.\n");
        return -ENOMEM;
    }
#endif

    // Initializes the driver.
    soft_uar_driver->owner                 = THIS_MODULE;
    soft_uar_driver->driver_name           = "soft_uar";
    soft_uar_driver->name                  = "ttyUAR";
    soft_uar_driver->major                 = SOFT_UAR_MAJOR;
    soft_uar_driver->minor_start           = 0;
    soft_uar_driver->flags                 = TTY_DRIVER_REAL_RAW;
    soft_uar_driver->type                  = TTY_DRIVER_TYPE_SERIAL;
    soft_uar_driver->subtype               = SERIAL_TYPE_NORMAL;
    soft_uar_driver->init_termios          = tty_std_termios;
    soft_uar_driver->init_termios.c_ispeed = 4800;
    soft_uar_driver->init_termios.c_ospeed = 4800;
    soft_uar_driver->init_termios.c_cflag  = B4800 | CREAD | CS8 | CLOCAL;

    // Sets the callbacks for the driver.
    tty_set_operations(soft_uar_driver, &soft_uar_operations);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
    // Link the port with the driver.
    tty_port_link_device(&port, soft_uar_driver, 0);
#endif

    // Registers the TTY driver.
    if (tty_register_driver(soft_uar_driver)) {
        printk(KERN_ALERT "soft_uar: Failed to register the driver.\n");
        put_tty_driver(soft_uar_driver);
        return -1; // return if registration fails
    }

    printk(KERN_INFO "soft_uar: Module initialized.\n");
    return 0;
}

/**
 * Cleanup function that gets called when the module is unloaded.
 */
static void __exit soft_uar_exit(void)
{
    printk(KERN_INFO "soft_uar: Finalizing the module...\n");

    // Finalizes the soft UAR.
    if (!__soft_uar_finalize())
    {
        printk(KERN_ALERT "soft_uar: Something went wrong whilst finalizing the soft UAR.\n");
    }

    // Unregisters the driver.
    if (tty_unregister_driver(soft_uar_driver))
    {
        printk(KERN_ALERT "soft_uar: Failed to unregister the driver.\n");
    }

    put_tty_driver(soft_uar_driver);
    printk(KERN_INFO "soft_uar: Module finalized.\n");
}

/**
 * Opens a given TTY device.
 * @param tty given TTY device
 * @param file
 * @return error code.
 */
static int soft_uar_open(struct tty_struct* tty, struct file* file)
{
    int error = NONE;

    if (__soft_uar_open(tty)) {
        printk(KERN_INFO "soft_uar: Device opened.\n");
    } else {
        printk(KERN_ALERT "soft_uar: Device busy.\n");
        error = -ENODEV;
    }

    return error;
}

/**
 * Closes a given TTY device.
 * @param tty
 * @param file
 */
static void soft_uar_close(struct tty_struct* tty, struct file* file)
{

    if (__soft_uar_close()) {
        printk(KERN_INFO "soft_uar: Device closed.\n");
    } else {
        printk(KERN_ALERT "soft_uar: Could not close the device.\n");
    }
}

/**
 * Writes the contents of a given buffer into a given TTY device.
 * @param tty given TTY device
 * @param buffer given buffer
 * @param buffer_size number of bytes contained in the given buffer
 * @return number of bytes successfuly written into the TTY device
 */
static int soft_uar_write(struct tty_struct* tty, const unsigned char* buffer, int buffer_size)
{
    return buffer_size;
}

/**
 * Does nothing.
 * @param tty
 */
static void soft_uar_flush_buffer(struct tty_struct* tty)
{
}

/**
 * Sets the UAR parameters for a given TTY (only the baudrate is taken into account).
 * @param tty given TTY
 * @param termios parameters
 */
static void soft_uar_set_termios(struct tty_struct* tty, struct ktermios* termios)
{
    int cflag = 0;
    speed_t baudrate = tty_get_baud_rate(tty);
    printk(KERN_INFO "soft_uar: soft_uar_set_termios: baudrate = %d.\n", baudrate);

    // Gets the cflag.
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
    cflag = tty->termios.c_cflag;
#else
    cflag = tty->termios->c_cflag;
#endif

    // Verifies the number of data bits (it must be 8).
    if ((cflag & CSIZE) != CS8) {
        printk(KERN_ALERT "soft_uar: Invalid number of data bits.\n");
    }

    // Verifies the number of stop bits (it must be 1).
    if (cflag & CSTOPB) {
        printk(KERN_ALERT "soft_uar: Invalid number of stop bits.\n");
    }

    // Verifies the parity (it must be none).
    if (cflag & PARENB) {
        printk(KERN_ALERT "soft_uar: Invalid parity.\n");
    }

    // Configure the baudrate.
    if (!__soft_uar_set_baudrate(baudrate)) {
        printk(KERN_ALERT "soft_uar: Invalid baudrate.\n");
    }
}

/**
 * Does nothing.
 * @param tty
 */
static void soft_uar_stop(struct tty_struct* tty)
{
    printk(KERN_DEBUG "soft_uar: soft_uar_stop.\n");
}

/**
 * Does nothing.
 * @param tty
 */
static void soft_uar_start(struct tty_struct* tty)
{
    printk(KERN_DEBUG "soft_uar: soft_uar_start.\n");
}

/**
 * Does nothing.
 * @param tty
 */
static void soft_uar_hangup(struct tty_struct* tty)
{
    printk(KERN_DEBUG "soft_uar: soft_uar_hangup.\n");
}

/**
 * Does nothing.
 * @param tty
 */
static int soft_uar_tiocmget(struct tty_struct* tty)
{
    return 0;
}

/**
 * Does nothing.
 * @param tty
 * @param set
 * @param clear
 */
static int soft_uar_tiocmset(struct tty_struct* tty, unsigned int set, unsigned int clear)
{
    return 0;
}

/**
 * Does nothing.
 * @param tty
 * @param command
 * @param parameter
 */
static int soft_uar_ioctl(struct tty_struct* tty, unsigned int command, unsigned int long parameter)
{
    int error = NONE;

    switch (command) {
    case TIOCMSET:
        error = NONE;
        break;

    case TIOCMGET:
        error = NONE;
        break;

    default:
        error = -ENOIOCTLCMD;
        break;
    }

    return error;
}

/**
 * Does nothing.
 * @param tty
 */

static void soft_uar_throttle(struct tty_struct* tty)
{
    printk(KERN_DEBUG "soft_uar: soft_uar_throttle.\n");
}

/**
 * Does nothing.
 * @param tty
 */
static void soft_uar_unthrottle(struct tty_struct* tty)
{
    printk(KERN_DEBUG "soft_uar: soft_uar_unthrottle.\n");
}

// Module entry points.
module_init(soft_uar_init);
module_exit(soft_uar_exit);
