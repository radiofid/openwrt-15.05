diff --git a/drivers/ata/libata-core.c b/drivers/ata/libata-core.c
index 54bdf83..4b3a63c 100644
--- a/drivers/ata/libata-core.c
+++ b/drivers/ata/libata-core.c
@@ -69,6 +69,7 @@
 #include <linux/ratelimit.h>
 #include <linux/pm_runtime.h>
 #include <linux/platform_device.h>
+#include <linux/leds.h>
 
 #include "libata.h"
 #include "libata-transport.h"
@@ -4950,6 +4951,9 @@ void ata_qc_complete(struct ata_queued_cmd *qc)
 {
 	struct ata_port *ap = qc->ap;
 
+	/* Trigger the LED (if avaiilable) */
+	ledtrig_disk_activity(!!(qc->tf.flags & ATA_TFLAG_WRITE));
+
 	/* XXX: New EH and old EH use different mechanisms to
 	 * synchronize EH with regular execution path.
 	 *
diff --git a/drivers/leds/Kconfig b/drivers/leds/Kconfig
index a210338..52b67c9 100644
--- a/drivers/leds/Kconfig
+++ b/drivers/leds/Kconfig
@@ -126,6 +126,13 @@ config LEDS_COBALT_RAQ
 	help
 	  This option enables support for the Cobalt Raq series LEDs.
 
+config LEDS_TRIGGER_DISK
+    bool "LEDS Disk Trigger"
+    depends on ATA
+    help
+      This allows LEDs to be controlled by disk activity.
+      If unsure, say Y.
+
 config LEDS_SUNFIRE
 	tristate "LED support for SunFire servers."
 	depends on LEDS_CLASS
diff --git a/drivers/leds/Makefile b/drivers/leds/Makefile
index ceb9eff..22d34ee 100644
--- a/drivers/leds/Makefile
+++ b/drivers/leds/Makefile
@@ -65,3 +65,4 @@ obj-$(CONFIG_LEDS_TRIGGERS)		+= trigger/
 obj-$(CONFIG_LEDS_TRIGGER_MORSE)	+= ledtrig-morse.o
 obj-$(CONFIG_LEDS_TRIGGER_NETDEV)	+= ledtrig-netdev.o
 obj-$(CONFIG_LEDS_TRIGGER_USBDEV)	+= ledtrig-usbdev.o
+obj-$(CONFIG_LEDS_TRIGGER_DISK)		+= ledtrig-disk.o
diff --git a/drivers/leds/ledtrig-disk.c b/drivers/leds/ledtrig-disk.c
new file mode 100644
index 0000000..8daf980
--- /dev/null
+++ b/drivers/leds/ledtrig-disk.c
@@ -0,0 +1,51 @@
+/*
+ * LED Disk Activity Trigger
+ *
+ * Copyright 2006 Openedhand Ltd.
+ *
+ * Author: Richard Purdie <rpurdie@openedhand.com>
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 as
+ * published by the Free Software Foundation.
+ *
+ */
+
+#include <linux/kernel.h>
+#include <linux/init.h>
+#include <linux/leds.h>
+
+#define BLINK_DELAY 30
+
+DEFINE_LED_TRIGGER(ledtrig_disk);
+DEFINE_LED_TRIGGER(ledtrig_disk_read);
+DEFINE_LED_TRIGGER(ledtrig_disk_write);
+DEFINE_LED_TRIGGER(ledtrig_ide);
+
+void ledtrig_disk_activity(bool write)
+{
+    unsigned long blink_delay = BLINK_DELAY;
+
+    led_trigger_blink_oneshot(ledtrig_disk,
+                              &blink_delay, &blink_delay, 0);
+    led_trigger_blink_oneshot(ledtrig_ide,
+                              &blink_delay, &blink_delay, 0);
+    if (write)
+        led_trigger_blink_oneshot(ledtrig_disk_write,
+                                  &blink_delay, &blink_delay, 0);
+    else
+        led_trigger_blink_oneshot(ledtrig_disk_read,
+                                  &blink_delay, &blink_delay, 0);
+}
+EXPORT_SYMBOL(ledtrig_disk_activity);
+
+static int __init ledtrig_disk_init(void)
+{
+    led_trigger_register_simple("disk-activity", &ledtrig_disk);
+    led_trigger_register_simple("disk-read", &ledtrig_disk_read);
+    led_trigger_register_simple("disk-write", &ledtrig_disk_write);
+    led_trigger_register_simple("ide-disk", &ledtrig_ide);
+
+    return 0;
+}
+device_initcall(ledtrig_disk_init);
diff --git a/include/linux/leds.h b/include/linux/leds.h
index a57611d..7375fc5 100644
--- a/include/linux/leds.h
+++ b/include/linux/leds.h
@@ -230,6 +230,12 @@ extern void ledtrig_ide_activity(void);
 static inline void ledtrig_ide_activity(void) {}
 #endif
 
+#ifdef CONFIG_LEDS_TRIGGER_DISK
+extern void ledtrig_disk_activity(bool write);
+#else
+static inline void ledtrig_disk_activity(bool write) {}
+#endif
+
 #if defined(CONFIG_LEDS_TRIGGER_CAMERA) || defined(CONFIG_LEDS_TRIGGER_CAMERA_MODULE)
 extern void ledtrig_flash_ctrl(bool on);
 extern void ledtrig_torch_ctrl(bool on);
