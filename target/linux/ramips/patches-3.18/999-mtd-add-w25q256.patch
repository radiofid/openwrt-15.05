diff --git a/drivers/mtd/spi-nor/spi-nor.c b/drivers/mtd/spi-nor/spi-nor.c
index de3ad4f..cda610d 100644
--- a/drivers/mtd/spi-nor/spi-nor.c
+++ b/drivers/mtd/spi-nor/spi-nor.c
@@ -28,6 +28,8 @@
 
 #define JEDEC_MFR(_jedec_id)	((_jedec_id) >> 16)
 
+#define SPI_NOR_MAX_ADDR_WIDTH	4
+
 static const struct spi_device_id *spi_nor_match_id(const char *name);
 
 /*
@@ -73,20 +75,29 @@ static int read_fsr(struct spi_nor *nor)
  * location. Return the configuration register value.
  * Returns negative if error occured.
  */
-static int read_cr(struct spi_nor *nor)
+static int _read_cr(struct spi_nor *nor, u8 reg)
 {
 	int ret;
 	u8 val;
 
-	ret = nor->read_reg(nor, SPINOR_OP_RDCR, &val, 1);
+	ret = nor->read_reg(nor, reg, &val, 1);
 	if (ret < 0) {
-		dev_err(nor->dev, "error %d reading CR\n", ret);
+		dev_err(nor->dev, "error %d reading %s\n", ret,
+			(reg==SPINOR_OP_RDCR)?"CR":"XCR");
 		return ret;
 	}
 
 	return val;
 }
 
+static inline int read_cr(struct spi_nor *nor) {
+	return _read_cr(nor, SPINOR_OP_RDCR);
+}
+
+static inline int read_xcr(struct spi_nor *nor) {
+	return _read_cr(nor, SPINOR_OP_RDXCR);
+}
+
 /*
  * Dummy Cycle calculation for different type of read.
  * It can be used to support more commands with
@@ -137,6 +148,72 @@ static inline struct spi_nor *mtd_to_spi_nor(struct mtd_info *mtd)
 	return mtd->priv;
 }
 
+static u8 spi_nor_convert_opcode(u8 opcode, const u8 table[][2], size_t size)
+{
+	size_t i;
+
+	for (i = 0; i < size; i++)
+		if (table[i][0] == opcode)
+			return table[i][1];
+
+	return opcode;
+}
+
+static inline u8 spi_nor_convert_3to4_read(u8 opcode)
+{
+	static const u8 spi_nor_3to4_read[][2] = {
+		{ SPINOR_OP_READ,			SPINOR_OP_READ4 },
+		{ SPINOR_OP_READ_FAST,		SPINOR_OP_READ4_FAST },
+		{ SPINOR_OP_READ_1_1_2,		SPINOR_OP_READ4_1_1_2 },
+		{ SPINOR_OP_READ_1_2_2,		SPINOR_OP_READ4_1_2_2 },
+		{ SPINOR_OP_READ_1_1_4,		SPINOR_OP_READ4_1_1_4 },
+		{ SPINOR_OP_READ_1_4_4,		SPINOR_OP_READ4_1_4_4 },
+	};
+	return spi_nor_convert_opcode(opcode, spi_nor_3to4_read,
+			ARRAY_SIZE(spi_nor_3to4_read));
+}
+
+struct flash_info;
+
+static void spi_nor_set_4byte_read(struct spi_nor *nor,
+				   const struct flash_info *info)
+{
+	nor->addr_width = 3;
+	nor->ext_addr = 0;
+	nor->read_opcode = spi_nor_convert_3to4_read(nor->read_opcode);
+	nor->flags |= SNOR_F_4B_EXT_ADDR;
+}
+
+static int spi_nor_check_ext_addr(struct spi_nor *nor, u32 addr)
+{
+	bool ext_addr;
+	int ret;
+	u8 cmd;
+
+	if (!(nor->flags & SNOR_F_4B_EXT_ADDR))
+		return 0;
+
+	ext_addr = !!(addr & 0xff000000);
+	if (nor->ext_addr == ext_addr)
+		return 0;
+
+	cmd = ext_addr ? SPINOR_OP_EN4B : SPINOR_OP_EX4B;
+	write_enable(nor);
+	ret = nor->write_reg(nor, cmd, NULL, 0, 0);
+	if (ret)
+		return ret;
+
+	cmd = 0;
+	ret = nor->write_reg(nor, SPINOR_OP_WREAR, &cmd, 1, 0);
+	if (ret)
+		return ret;
+
+	nor->addr_width = 3 + ext_addr;
+	nor->ext_addr = ext_addr;
+	write_disable(nor);
+	return 0;
+}
+
 /* Enable/disable 4-byte addressing mode. */
 static inline int set_4byte(struct spi_nor *nor, u32 jedec_id, int enable)
 {
@@ -268,6 +345,29 @@ static void spi_nor_unlock_and_unprep(struct spi_nor *nor, enum spi_nor_ops ops)
 }
 
 /*
+ * Initiate the erasure of a single sector
+ */
+static int spi_nor_erase_sector(struct spi_nor *nor, u32 addr)
+{
+	u8 buf[SPI_NOR_MAX_ADDR_WIDTH];
+	int i;
+
+	if (nor->erase)
+		return nor->erase(nor, addr);
+
+	/*
+	 * Default implementation, if driver doesn't have a specialized HW
+	 * control
+	 */
+	for (i = nor->addr_width - 1; i >= 0; i--) {
+		buf[i] = addr & 0xff;
+		addr >>= 8;
+	}
+
+	return nor->write_reg(nor, nor->erase_opcode, buf, nor->addr_width, 0);
+}
+
+/*
  * Erase an address range on the nor chip.  The address range may extend
  * one or more erase sectors.  Return an error is there is a problem erasing.
  */
@@ -292,8 +392,14 @@ static int spi_nor_erase(struct mtd_info *mtd, struct erase_info *instr)
 	if (ret)
 		return ret;
 
+    ret = spi_nor_check_ext_addr(nor, addr + len);
+	if (ret)
+		return ret;
+
 	/* whole-chip erase? */
 	if (len == mtd->size) {
+		write_enable(nor);
+
 		if (erase_chip(nor)) {
 			ret = -EIO;
 			goto erase_err;
@@ -307,27 +413,30 @@ static int spi_nor_erase(struct mtd_info *mtd, struct erase_info *instr)
 	/* "sector"-at-a-time erase */
 	} else {
 		while (len) {
-			if (nor->erase(nor, addr)) {
-				ret = -EIO;
+			write_enable(nor);
+
+			ret = spi_nor_erase_sector(nor, addr);
+			if (ret)
 				goto erase_err;
-			}
 
 			addr += mtd->erasesize;
 			len -= mtd->erasesize;
+
+			ret = spi_nor_wait_till_ready(nor);
+			if (ret)
+				goto erase_err;
 		}
 	}
+	write_disable(nor);
 
+erase_err:
+	spi_nor_check_ext_addr(nor, 0);
 	spi_nor_unlock_and_unprep(nor, SPI_NOR_OPS_ERASE);
 
-	instr->state = MTD_ERASE_DONE;
+	instr->state = ret ? MTD_ERASE_FAILED : MTD_ERASE_DONE;
 	mtd_erase_callback(instr);
 
 	return ret;
-
-erase_err:
-	spi_nor_unlock_and_unprep(nor, SPI_NOR_OPS_ERASE);
-	instr->state = MTD_ERASE_FAILED;
-	return ret;
 }
 
 static int spi_nor_lock(struct mtd_info *mtd, loff_t ofs, uint64_t len)
@@ -450,6 +559,10 @@ struct flash_info {
 #define	SPI_NOR_DUAL_READ	0x20    /* Flash supports Dual Read */
 #define	SPI_NOR_QUAD_READ	0x40    /* Flash supports Quad Read */
 #define	USE_FSR			0x80	/* use flag status register */
+#define SPI_NOR_4B_READ_OP	BIT(15)	/*
+					 * Like SPI_NOR_4B_OPCODES, but for read
+					 * op code only.
+					 */
 };
 
 #define INFO(_jedec_id, _ext_id, _sector_size, _n_sectors, _flags)	\
@@ -636,7 +749,7 @@ static const struct spi_device_id spi_nor_ids[] = {
 	{ "w25q80", INFO(0xef5014, 0, 64 * 1024,  16, SECT_4K) },
 	{ "w25q80bl", INFO(0xef4014, 0, 64 * 1024,  16, SECT_4K) },
 	{ "w25q128", INFO(0xef4018, 0, 64 * 1024, 256, SECT_4K) },
-	{ "w25q256", INFO(0xef4019, 0, 64 * 1024, 512, SECT_4K) },
+	{ "w25q256", INFO(0xef4019, 0, 64 * 1024, 512, SECT_4K | SPI_NOR_4B_READ_OP) },
 
 	/* Catalyst / On Semiconductor -- non-JEDEC */
 	{ "cat25c11", CAT25_INFO(  16, 8, 16, 1, SPI_NOR_NO_ERASE | SPI_NOR_NO_FR) },
@@ -691,8 +804,23 @@ static int spi_nor_read(struct mtd_info *mtd, loff_t from, size_t len,
 	if (ret)
 		return ret;
 
+	if (nor->flags & SNOR_F_4B_EXT_ADDR)
+		nor->addr_width = 4;
+
 	ret = nor->read(nor, from, len, retlen, buf);
 
+	if (nor->flags & SNOR_F_4B_EXT_ADDR) {
+		u8 val = 0;
+
+		if ((from + len) & 0xff000000) {
+			write_enable(nor);
+			nor->write_reg(nor, SPINOR_OP_WREAR, &val, 1, 0);
+			write_disable(nor);
+		}
+
+		nor->addr_width = 3;
+	}
+
 	spi_nor_unlock_and_unprep(nor, SPI_NOR_OPS_READ);
 	return ret;
 }
@@ -777,8 +905,8 @@ static int spi_nor_write(struct mtd_info *mtd, loff_t to, size_t len,
 	size_t *retlen, const u_char *buf)
 {
 	struct spi_nor *nor = mtd_to_spi_nor(mtd);
-	u32 page_offset, page_size, i;
-	int ret;
+	u32 page_offset, page_remain, i;
+	int ret = 0;
 
 	dev_dbg(nor->dev, "to 0x%08x, len %zd\n", (u32)to, len);
 
@@ -786,39 +914,43 @@ static int spi_nor_write(struct mtd_info *mtd, loff_t to, size_t len,
 	if (ret)
 		return ret;
 
-	/* Wait until finished previous write command. */
-	ret = wait_till_ready(nor);
-	if (ret)
-		goto write_err;
+	ret = spi_nor_check_ext_addr(nor, to + len);
+	if (ret < 0)
+		return ret;
 
-	write_enable(nor);
+	for (i = 0; i < len;) {
+		ssize_t written;
+		loff_t addr = to + i;
 
-	page_offset = to & (nor->page_size - 1);
+		if (hweight32(nor->page_size) == 1) {
+			page_offset = addr & (nor->page_size - 1);
+		} else {
+			uint64_t aux = addr;
+			page_offset = do_div(aux, nor->page_size);
+		}
 
-	/* do all the bytes fit onto one page? */
-	if (page_offset + len <= nor->page_size) {
-		nor->write(nor, to, len, retlen, buf);
-	} else {
-		/* the size of data remaining on the first page */
-		page_size = nor->page_size - page_offset;
-		nor->write(nor, to, page_size, retlen, buf);
+		page_remain = min_t(size_t,
+				nor->page_size - page_offset, len - i);
 
-		/* write everything in nor->page_size chunks */
-		for (i = page_size; i < len; i += page_size) {
-			page_size = len - i;
-			if (page_size > nor->page_size)
-				page_size = nor->page_size;
+		write_enable(nor);
+		nor->write(nor, addr, page_remain, &ret, buf + i);
+		if (ret <= 0)
+			goto write_err;
 
-			wait_till_ready(nor);
-			write_enable(nor);
+		written = ret;
 
-			nor->write(nor, to + i, page_size, retlen, buf + i);
-		}
+		ret = spi_nor_wait_till_ready(nor);
+		if (ret)
+			goto write_err;
+
+		*retlen += written;
+		i += written;
 	}
 
 write_err:
+	spi_nor_check_ext_addr(nor, 0);
 	spi_nor_unlock_and_unprep(nor, SPI_NOR_OPS_WRITE);
-	return 0;
+	return ret;
 }
 
 static int macronix_quad_enable(struct spi_nor *nor)
@@ -1095,6 +1227,15 @@ int spi_nor_scan(struct spi_nor *nor, const char *name, enum read_mode mode)
 			/* No small sector erase for 4-byte command set */
 			nor->erase_opcode = SPINOR_OP_SE_4B;
 			mtd->erasesize = info->sector_size;
+		} else if (info->flags & SPI_NOR_4B_READ_OP &&
+				(JEDEC_MFR(info->jedec_id) == 0xef || 
+                 JEDEC_MFR(info->jedec_id) == 0xc2)) {
+			ret = read_xcr(nor);
+			if (!(ret > 0 && (ret & XCR_DEF_4B_ADDR_MODE))) {
+				dev_warn(dev, "setting 4byte read and 3byte write\n");
+				spi_nor_set_4byte_read(nor, info);
+			} else
+				set_4byte(nor, info->jedec_id, 1);
 		} else
 			set_4byte(nor, info->jedec_id, 1);
 	} else {
diff --git a/include/linux/mtd/spi-nor.h b/include/linux/mtd/spi-nor.h
index 046a0a2..ce686b5 100644
--- a/include/linux/mtd/spi-nor.h
+++ b/include/linux/mtd/spi-nor.h
@@ -25,7 +25,9 @@
 #define SPINOR_OP_READ		0x03	/* Read data bytes (low frequency) */
 #define SPINOR_OP_READ_FAST	0x0b	/* Read data bytes (high frequency) */
 #define SPINOR_OP_READ_1_1_2	0x3b	/* Read data bytes (Dual SPI) */
+#define SPINOR_OP_READ_1_2_2	0xbb	/* Read data bytes (Dual SPI) */
 #define SPINOR_OP_READ_1_1_4	0x6b	/* Read data bytes (Quad SPI) */
+#define SPINOR_OP_READ_1_4_4	0xeb	/* Read data bytes (Quad SPI) */
 #define SPINOR_OP_PP		0x02	/* Page program (up to 256 bytes) */
 #define SPINOR_OP_BE_4K		0x20	/* Erase 4KiB block */
 #define SPINOR_OP_BE_4K_PMC	0xd7	/* Erase 4KiB block on PMC chips */
@@ -34,13 +36,16 @@
 #define SPINOR_OP_SE		0xd8	/* Sector erase (usually 64KiB) */
 #define SPINOR_OP_RDID		0x9f	/* Read JEDEC ID */
 #define SPINOR_OP_RDCR		0x35	/* Read configuration register */
+#define SPINOR_OP_RDXCR		0x15	/* Read configuration register */
 #define SPINOR_OP_RDFSR		0x70	/* Read flag status register */
 
 /* 4-byte address opcodes - used on Spansion and some Macronix flashes. */
 #define SPINOR_OP_READ4		0x13	/* Read data bytes (low frequency) */
 #define SPINOR_OP_READ4_FAST	0x0c	/* Read data bytes (high frequency) */
 #define SPINOR_OP_READ4_1_1_2	0x3c	/* Read data bytes (Dual SPI) */
+#define SPINOR_OP_READ4_1_2_2	0xbc	/* Read data bytes (Dual SPI) */
 #define SPINOR_OP_READ4_1_1_4	0x6c	/* Read data bytes (Quad SPI) */
+#define SPINOR_OP_READ4_1_4_4	0xec	/* Read data bytes (Quad SPI) */
 #define SPINOR_OP_PP_4B		0x12	/* Page program (up to 256 bytes) */
 #define SPINOR_OP_SE_4B		0xdc	/* Sector erase (usually 64KiB) */
 
@@ -52,6 +57,7 @@
 /* Used for Macronix and Winbond flashes. */
 #define SPINOR_OP_EN4B		0xb7	/* Enter 4-byte mode */
 #define SPINOR_OP_EX4B		0xe9	/* Exit 4-byte mode */
+#define SPINOR_OP_WREAR		0xc5
 
 /* Used for Spansion flashes only. */
 #define SPINOR_OP_BRWR		0x17	/* Bank register write */
@@ -72,6 +78,7 @@
 
 /* Configuration Register bits. */
 #define CR_QUAD_EN_SPAN		0x2	/* Spansion Quad I/O */
+#define XCR_DEF_4B_ADDR_MODE 0x2 /* Winbond 4B mode default */
 
 enum read_mode {
 	SPI_NOR_NORMAL = 0,
@@ -116,6 +123,10 @@ enum spi_nor_ops {
 	SPI_NOR_OPS_UNLOCK,
 };
 
+enum spi_nor_option_flags {
+	SNOR_F_4B_EXT_ADDR			= BIT(6),
+};
+
 /**
  * struct spi_nor - Structure for defining a the SPI NOR layer
  * @mtd:		point to a mtd_info structure
@@ -161,6 +172,8 @@ struct spi_nor {
 	enum read_mode		flash_read;
 	bool			sst_write_second;
 	struct spi_nor_xfer_cfg	cfg;
+	u32			flags;
+	u8			ext_addr;
 	u8			cmd_buf[SPI_NOR_MAX_CMD_SIZE];
 
 	int (*prepare)(struct spi_nor *nor, enum spi_nor_ops ops);
