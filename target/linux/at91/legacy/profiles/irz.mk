#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/IRZ_RUH2B
	NAME:=IRZ RUH2b
endef
define Profile/IRZ_RUH2B/Description
	IRZ RUH2b router board
endef
$(eval $(call Profile,IRZ_RUH2B))

define Profile/IRZ_RUH3
	NAME:=IRZ RUH3
endef
define Profile/IRZ_RUH3/Description
	IRZ RUH3 router board
endef
$(eval $(call Profile,IRZ_RUH3))



